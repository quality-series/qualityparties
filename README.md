# QualityParties  
[![pipeline status](https://gitlab.com/quality-series/qualityparties/badges/master/pipeline.svg)](https://gitlab.com/quality-series/qualityparties/-/commits/master)

This plugin is not intended to solely be used independently, but to be expanded upon with the work of other developers by utilizing documentation to modify messages and allow for party members to complete actions together.  

For documentation, check out the [wiki page](https://gitlab.com/quality-series/qualityparties/-/wikis/home).

## Features
  - Command auto-completion for all arguments
  - Fully customizable messages, tooltips, and hover events
  - Open Source & Free
  - Complete Documentation
  - Easy-Use Developer API

## Required Dependencies
  - [PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/)

## Commands & Permissions
| command | permission | description | aliases |
| ------ | ------ | ------ | ------ |
| `/party [player]` | `qualityparties.user` | Displays the help message or invites a specified player | `/p [player]` |
| `/party help` | `qualityparties.user` | Displays the help message | `/p ?` |
| `/party invite <player>` | `qualityparties.user` | Invites a specified player | `/p inv <player>`, `/p add <player>` |
| `/party accept [player]` | `qualityparties.user` | Accepts the latest or specified invite | `/p join [player]` |
| `/party decline [player]` | `qualityparties.user` | Declines the latest or specified invite | `/p reject [player]`, `/p deny [player]` |
| `/party list` | `qualityparties.user` | Displays information about the sender's current party | `/p info` |
| `/party leave` | `qualityparties.user` | Causes the sender to leave their current party | `/p quit` |
| `/party disband` | `qualityparties.user` | Disbands the current party, if the sender is the leader | `/p disband` |
| `/party transfer <player>` | `qualityparties.user` | Transfers leadership of the current party, if sender is leader | `/p transfer <player>` |
| `/party kick <player>` | `qualityparties.user` | Kicks a player from current party if you're leader | `/p kick <player>` |
| `/party forcejoin <player>` | `qualityparties.admin.forcejoin` | Forcefully joins a player's party | `/p forcejoin <player>` |
| `/party forceleader` | `qualityparties.admin.forceleader` | Forcefully makes the sender the leader of their current party | `/p hijack` |
| `/qualityparties` | `qualityparties.admin` | Displays information about the plugin | `/qp` |
| `/qualityparties reload` | `qualityparties.admin.reload` | Reloads configuration and language files | `/qp reload` |


## Installation
  - Make sure you have [PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/) installed.
  - Ensure you have the `Player` module of PlaceholderAPI downloaded. See [here](https://gitlab.com/quality-series/qualityparties/-/wikis/Configuration#placeholderapi) for a tutorial.  
  - Download QualityParties and put it in your `plugins` folder.
  - Restart your server
  - Profit - everything should be up and running great!

## Libraries Utilized
  - [Drink](https://github.com/jonahseguin/drink) - (command syntax + easy argument parsing)
  - [SpaceAPI](https://gitlab.com/space-series/spaceapi) - (messages & configurations)
  - [PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/) - (placeholder support)


