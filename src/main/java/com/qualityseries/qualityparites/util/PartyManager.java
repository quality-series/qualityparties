package com.qualityseries.qualityparites.util;

import com.qualityseries.qualityparites.Messages;
import com.qualityseries.qualityparites.QualityParties;
import com.qualityseries.qualityparites.config.Config;
import com.qualityseries.qualityparites.models.Party;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static com.qualityseries.qualityparites.config.Config.PLAYER_CHAT_STRUCTURE;

public class PartyManager {

    public void createParty(Player player) {
        Party party = new Party(player);
        QualityParties.getInstance().getParties().put(party.getUuid(), party);
    }

    public void createParty(Player leader, ArrayList<Player> members) {
        Party party = new Party(leader);
        party.getMembers().addAll(members);
        QualityParties.getInstance().getParties().put(party.getUuid(), party);
    }

    public Party getPartyByLeader(Player player) {
        for (UUID uuid : QualityParties.getInstance().getParties().keySet()) {
            Party party = QualityParties.getInstance().getParties().get(uuid);
            if (party.getLeader() == player) return party;
        }
        return null;
    }

    public Party getPartyByMember(Player player) {
        for (UUID uuid : QualityParties.getInstance().getParties().keySet()) {
            Party party = QualityParties.getInstance().getParties().get(uuid);
            if (party.getMembers().contains(player)) return party;
        }
        return null;
    }

    public Party getPartyByPlayer(Player player) {
        if (getPartyByLeader(player) != null) return getPartyByLeader(player);
        if (getPartyByMember(player) != null) return getPartyByMember(player);
        return null;
    }

    /**
     * Deletes a party (includes pending invites)
     * @param party the party to delete
     */
    private void deleteParty(Party party) {
        for (Player player : QualityParties.getInstance().getPendingInvites().keySet()) QualityParties.getInstance().getPendingInvites().get(player).remove(party);
        QualityParties.getInstance().getParties().remove(party.getUuid());
    }

    /**
     * Invites a player to join a party
     * @param leader The leader of the party
     * @param target The player the leader is inviting
     */
    public void invite(Player leader, Player target) {
        // should never occur, but good to check
        if (target == null) return;

        Party party = getPartyByLeader(leader);

        // stop the leader from inviting themself
        if (target.getUniqueId().toString().equals(leader.getUniqueId().toString())) {
            Messages.getInstance().cantInviteSelf.msg(QualityParties.getInstance().getSpacePlugin().getSender(leader));
            return;
        }

        // if the party isn't null, we need to do some checks to make sure that the leader isn't doing anything silly
        if (party != null) {
            // if player is in the party at all, we don't need to invite them
            for (Player partyPlayer : party.getAll()) {
                if (partyPlayer.getUniqueId().toString().equals(target.toString())) {
                    Messages.getInstance().playerAlreadyInParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(leader),
                            "{0}", PlaceholderAPI.setPlaceholders(target, PLAYER_CHAT_STRUCTURE.get(Config.get())));
                    return;
                }
            }

            if (QualityParties.getInstance().getPendingInvites().get(target) != null) {
                // if the user already has a pending invite, don't send a new one
                if (QualityParties.getInstance().getPendingInvites().get(target).contains(party)) {
                    Messages.getInstance().playerAlreadyHasPendingInvite.msg(QualityParties.getInstance().getSpacePlugin().getSender(leader),
                            "{0}", PlaceholderAPI.setPlaceholders(target, PLAYER_CHAT_STRUCTURE.get(Config.get())));
                    return;
                }
            }
        } else {
            createParty(leader);

            // redefine the party as now they will have one
            party = getPartyByLeader(leader);
        }

        // add the invite to the list of the player's pending invites
        if (QualityParties.getInstance().getPendingInvites().get(target) != null) {
            QualityParties.getInstance().getPendingInvites().get(target).add(party);
        } else {
            QualityParties.getInstance().getPendingInvites().put(target, new ArrayList<>(Collections.singletonList(party)));
        }

        // Message everyone (party & invited user) about what's happening
        party.getAll().forEach(player -> Messages.getInstance().inviteToParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(player),
                "{0}", PlaceholderAPI.setPlaceholders(leader, PLAYER_CHAT_STRUCTURE.get(Config.get())),
                "{1}", PlaceholderAPI.setPlaceholders(target, PLAYER_CHAT_STRUCTURE.get(Config.get()))));

        Messages.getInstance().inviteToInvited.msg(QualityParties.getInstance().getSpacePlugin().getSender(target),
                "{0}", PlaceholderAPI.setPlaceholders(leader, PLAYER_CHAT_STRUCTURE.get(Config.get())));

        AtomicInteger counter = new AtomicInteger(0);

        Party finalParty = party;
        new BukkitRunnable() {
            @Override
            public void run() {
                counter.incrementAndGet();

                // check to see if the player has denied the party invite and cancel it
                // no need to send messages here as those will be handled in the actual deny logic
                if (!QualityParties.getInstance().getPendingInvites().get(target).contains(finalParty)) cancel();

                // if a minute has passed, cancel the invite and notify the party and the target
                if (counter.get() > 59) {
                    finalParty.getAll().forEach(player -> Messages.getInstance().inviteExpiredToParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(player),
                            "{0}", PlaceholderAPI.setPlaceholders(target, PLAYER_CHAT_STRUCTURE.get(Config.get()))));

                    Messages.getInstance().inviteExpiredToInvited.msg(QualityParties.getInstance().getSpacePlugin().getSender(target),
                            "{0}", PlaceholderAPI.setPlaceholders(leader, PLAYER_CHAT_STRUCTURE.get(Config.get())));

                    QualityParties.getInstance().getPendingInvites().get(target).remove(finalParty);

                    cancel();
                }
            }
        }.runTaskTimerAsynchronously(QualityParties.getInstance(), 0L, 20L);
    }

    /**
     * Declines an invite
     * @param player The player who is declining the invite
     * @param party The party to which the player is declining the invite from
     */
    public void declineInvite(Player player, Party party) {
        // if the party has been disbanded since the player has been invited
        if (party == null) {
            Messages.getInstance().noPartyExist.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }

        // if the player doesn't have any pending invites
        if (!QualityParties.getInstance().getPendingInvites().containsKey(player) || QualityParties.getInstance().getPendingInvites().get(player).isEmpty()) {
            Messages.getInstance().noPendingInvites.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }

        // if the player doesn't have an invite from the specified party
        if (!QualityParties.getInstance().getPendingInvites().get(player).contains(party)) {
            Messages.getInstance().noInvitation.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
        }

        // remove the invite from the party
        QualityParties.getInstance().getPendingInvites().get(player).remove(party);

        // message everyone about the player declining
        party.getAll().forEach(member -> Messages.getInstance().playerDeclinedToParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(member),
                "{0}", PlaceholderAPI.setPlaceholders(player, PLAYER_CHAT_STRUCTURE.get(Config.get()))));

        Messages.getInstance().playerDeclinedToPlayer.msg(QualityParties.getInstance().getSpacePlugin().getSender(player),
                "{0}", PlaceholderAPI.setPlaceholders(party.getLeader(), PLAYER_CHAT_STRUCTURE.get(Config.get())));
    }

    /**
     * Declines the latest invite a player has received
     * @param player The player that is declining an invite
     */
    public void declineInvite(Player player) {
        if (!QualityParties.getInstance().getPendingInvites().containsKey(player) || QualityParties.getInstance().getPendingInvites().get(player).isEmpty()) {
            Messages.getInstance().noPendingInvites.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }
        ArrayList<Party> parties = QualityParties.getInstance().getPendingInvites().get(player);
        declineInvite(player, parties.get(parties.size() - 1));
    }

    /**
     * Accepts an invite
     * @param player The player accepting the invite
     * @param party The party that the invite is coming from
     */
    public void acceptInvite(Player player, Party party) {
        // if the party has been disbanded since the player has been invited
        if (party == null) {
            Messages.getInstance().noPartyExist.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }

        // if the player doesn't have any pending invites
        if (!QualityParties.getInstance().getPendingInvites().containsKey(player) || QualityParties.getInstance().getPendingInvites().get(player).isEmpty()) {
            Messages.getInstance().noPendingInvites.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }

        // if the player doesn't have an invite from the specified party
        if (!QualityParties.getInstance().getPendingInvites().get(player).contains(party)) {
            Messages.getInstance().noInvitation.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
        }

        // remove the invite from the party
        QualityParties.getInstance().getPendingInvites().get(player).remove(party);

        // have the player leave any other party they are in
        if (getPartyByPlayer(player) != null) leave(player);

        // message about the player joining the party
        party.getAll().forEach(member -> Messages.getInstance().playerJoinedPartyToParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(member),
                "{0}", PlaceholderAPI.setPlaceholders(player, PLAYER_CHAT_STRUCTURE.get(Config.get()))));

        Messages.getInstance().playerJoinedPartyToPlayer.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));

        party.addMembers(player);
    }

    /**
     * Accepts the latest invite a player has received
     * @param player The player accepting the invite
     */
    public void acceptInvite(Player player) {
        if (!QualityParties.getInstance().getPendingInvites().containsKey(player) || QualityParties.getInstance().getPendingInvites().get(player).isEmpty()) {
            Messages.getInstance().noPendingInvites.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }
        ArrayList<Party> parties = QualityParties.getInstance().getPendingInvites().get(player);
        acceptInvite(player, parties.get(parties.size() - 1));
    }

    /**
     * Disbands a party
     * @param player the leader of the party to disband
     */
    public void disband(Player player) {
        Party party = getPartyByLeader(player);

        // check if the player is a leader of a party
        if (party == null) {
            Messages.getInstance().notLeader.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }

        party.getAll().forEach(member -> Messages.getInstance().disband.msg(QualityParties.getInstance().getSpacePlugin().getSender(member)));
        deleteParty(party);
    }

    /**
     * Removes a player from a party (of their own accord)
     * @param player the player who left
     */
    public void leave(Player player) {
        if (getPartyByLeader(player) == null && getPartyByMember(player) == null) {
            Messages.getInstance().noParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }

        Party party;

        if (getPartyByLeader(player) != null) party = getPartyByLeader(player); else party = getPartyByMember(player);

        if (party.getLeader().getUniqueId().toString().equals(player.getUniqueId().toString()) && party.getAll().size() > 2) party.transferLeader(party.getMembers().get(0));

        party.removeMembers(player);

        Messages.getInstance().playerLeftToLeaver.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));

        party.getAll().forEach(member -> {
            if (!member.getUniqueId().toString().equals(player.getUniqueId().toString())) {
                Messages.getInstance().playerLeftToParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(member),
                        "{0}", PlaceholderAPI.setPlaceholders(player, PLAYER_CHAT_STRUCTURE.get(Config.get())));
            }
        });

        if (party.getAll().size() < 2) {
            disband(party.getLeader());
        }
    }

    /**
     * Transfers leadership of the party to another party member
     * @param leader The leader of the party
     * @param target The person to become the leader
     */
    public void transfer(Player leader, Player target) {
        // check if the supposed leader is even a leader
        if (getPartyByLeader(leader) == null) {
            Messages.getInstance().notLeader.msg(QualityParties.getInstance().getSpacePlugin().getSender(leader));
            return;
        }

        Party party = getPartyByLeader(leader);

        if (!party.getMembers().contains(target)) {
            Messages.getInstance().playerNotFound.msg(QualityParties.getInstance().getSpacePlugin().getSender(leader),
                    "{0}", PlaceholderAPI.setPlaceholders(target, PLAYER_CHAT_STRUCTURE.get(Config.get())));
            return;
        }

        party.transferLeader(target);

        party.getAll().forEach(player -> Messages.getInstance().transferLeader.msg(QualityParties.getInstance().getSpacePlugin().getSender(player),
                "{0}", PlaceholderAPI.setPlaceholders(leader, PLAYER_CHAT_STRUCTURE.get(Config.get())),
                "{1}", PlaceholderAPI.setPlaceholders(target, PLAYER_CHAT_STRUCTURE.get(Config.get()))));
    }

    /**
     * Sends information on the members of a party
     * @param player The person to send the information to
     */
    public void info(Player player) {
        if (getPartyByPlayer(player) == null) {
            Messages.getInstance().noParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }

        Party party = getPartyByPlayer(player);

        StringBuilder sb = new StringBuilder();
        for (Player member : party.getMembers()) {
            sb.append(PlaceholderAPI.setPlaceholders(member, PLAYER_CHAT_STRUCTURE.get(Config.get()))).append(", ");
        }
        String result = sb.deleteCharAt(sb.length() - 1).toString();

        Messages.getInstance().partyInfo.msg(QualityParties.getInstance().getSpacePlugin().getSender(player), "{0}", PlaceholderAPI.setPlaceholders(party.getLeader(), PLAYER_CHAT_STRUCTURE.get(Config.get())),
                "{1}", String.valueOf(party.getMembers().size()), "{2}", result);
    }

    /**
     * Kicks a player from a party
     * @param leader The leader of the party
     * @param target The target of the kick
     */
    public void kick(Player leader, Player target) {
        if (getPartyByLeader(leader) == null) {
            Messages.getInstance().notLeader.msg(QualityParties.getInstance().getSpacePlugin().getSender(leader));
            return;
        }

        Party party = getPartyByLeader(leader);

        if (!party.getMembers().contains(target)) {
            Messages.getInstance().playerNotFound.msg(QualityParties.getInstance().getSpacePlugin().getSender(leader),
                    "{0}", PlaceholderAPI.setPlaceholders(target, PLAYER_CHAT_STRUCTURE.get(Config.get())));
            return;
        }

        party.removeMembers(target);

        party.getAll().forEach(member -> Messages.getInstance().playerKickedToParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(member),
                "{0}", PlaceholderAPI.setPlaceholders(target, PLAYER_CHAT_STRUCTURE.get(Config.get()))));

        Messages.getInstance().playerKickedToKicked.msg(QualityParties.getInstance().getSpacePlugin().getSender(target));
    }

    /**
     * Forces a player to become their party's leader
     * @param player The player to become the leader
     */
    public void forceLeader(Player player) {
        // Make sure the player is not already the leader
        if (getPartyByLeader(player) != null) {
            Messages.getInstance().alreadyLeader.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }

        // Make sure the player has a party
        if (getPartyByMember(player) == null) {
            Messages.getInstance().noParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }

        Party party = getPartyByMember(player);

        party.transferLeader(player);

        party.getAll().forEach(member -> Messages.getInstance().hijack.msg(QualityParties.getInstance().getSpacePlugin().getSender(member),
                "{0}", PlaceholderAPI.setPlaceholders(player, PLAYER_CHAT_STRUCTURE.get(Config.get()))));
    }

    /**
     * Forces a player to join a party
     * @param joiner The player who will join
     * @param member The member of the party to join
     */
    public void forceJoin(Player joiner, Player member) {
        // check to make sure the member has a party
        if (getPartyByPlayer(member) == null) {
            Messages.getInstance().noPartyExist.msg(QualityParties.getInstance().getSpacePlugin().getSender(joiner));
            return;
        }

        // if the joiner has a current party, make sure it isn't the one they're trying to join
        if (getPartyByPlayer(joiner) != null) {
            if (getPartyByPlayer(joiner).getUuid().toString().equals(getPartyByPlayer(member).getUuid().toString())) {
                Messages.getInstance().alreadyInParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(joiner));
                return;
            }
        }

        Party party = getPartyByPlayer(member);

        // have the player leave any other party they are in
        if (getPartyByPlayer(joiner) != null) leave(joiner);

        // message about the player joining the party
        party.getAll().forEach(user -> Messages.getInstance().playerJoinedPartyToParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(user),
                "{0}", PlaceholderAPI.setPlaceholders(joiner, PLAYER_CHAT_STRUCTURE.get(Config.get()))));

        Messages.getInstance().playerJoinedPartyToPlayer.msg(QualityParties.getInstance().getSpacePlugin().getSender(joiner));

        party.addMembers(joiner);
    }
}

