package com.qualityseries.qualityparites;

import dev.spaceseries.spaceapi.config.impl.Configuration;
import dev.spaceseries.spaceapi.text.Message;

public class Messages {

    /**
     * Instance of this class
     */
    private static Messages instance;

    /**
     * Gets instance of messages class
     * <p>(Singleton)</p>
     *
     * @return messages
     */
    public static Messages getInstance() {
        if (instance == null)
            instance = new Messages();
        return instance;
    }


    /**
     * Renews the messages
     */
    public static void renew() {
        instance = null;
    }

    // main

    public Message invalidSyntax = Message.fromConfigurationSection(getLangConfiguration().getSection("main.invalid-syntax"), "invalid-syntax")
            .build();

    public Message playerNotFound = Message.fromConfigurationSection(getLangConfiguration().getSection("main.player-not-found"), "player-not-found")
            .build();

    public Message cantInviteSelf = Message.fromConfigurationSection(getLangConfiguration().getSection("main.cant-invite-self"), "main.cant-invite-self")
            .build();

    public Message partyHelp = Message.fromConfigurationSection(getLangConfiguration().getSection("party.help"), "party.help")
            .build();

    // invites

    public Message inviteToInvited = Message.fromConfigurationSection(getLangConfiguration().getSection("party.invite.to-invited"), "party.invite.to-invited")
            .build();

    public Message inviteToParty = Message.fromConfigurationSection(getLangConfiguration().getSection("party.invite.to-party"), "party.invite.to-party")
            .build();

    public Message inviteExpiredToInvited = Message.fromConfigurationSection(getLangConfiguration().getSection("party.invite.expired-to-invited"), "party.invite.expired-to-invited")
            .build();

    public Message inviteExpiredToParty = Message.fromConfigurationSection(getLangConfiguration().getSection("party.invite.expired-to-party"), "party.invite.expired-to-party")
            .build();

    public Message playerAlreadyHasPendingInvite = Message.fromConfigurationSection(getLangConfiguration().getSection("party.invite.already-pending"), "party.invite.already-pending")
            .build();

    public Message playerAlreadyInParty = Message.fromConfigurationSection(getLangConfiguration().getSection("party.invite.already-in-party"), "party.invite.already-in-party")
            .build();

    public Message noPendingInvites = Message.fromConfigurationSection(getLangConfiguration().getSection("party.invite.none-pending"), "party.invite.none-pending")
            .build();

    public Message playerDeclinedToPlayer = Message.fromConfigurationSection(getLangConfiguration().getSection("party.invite.declined-to-player"), "party.invite.declined-to-player")
            .build();

    public Message playerDeclinedToParty = Message.fromConfigurationSection(getLangConfiguration().getSection("party.invite.declined-to-party"), "party.invite.declined-to-party")
            .build();

    public Message noInvitation = Message.fromConfigurationSection(getLangConfiguration().getSection("party.invite.no-invitation"), "party.invite.no-invitation")
            .build();

    // join

    public Message playerJoinedPartyToPlayer = Message.fromConfigurationSection(getLangConfiguration().getSection("party.join.to-player"), "party.join.to-player")
            .build();

    public Message playerJoinedPartyToParty = Message.fromConfigurationSection(getLangConfiguration().getSection("party.join.to-party"), "party.join.to-party")
            .build();

    // leave

    public Message playerLeftToLeaver = Message.fromConfigurationSection(getLangConfiguration().getSection("party.leave.to-leaver"), "party.leave.to-leaver")
            .build();

    public Message playerLeftToParty = Message.fromConfigurationSection(getLangConfiguration().getSection("party.leave.to-party"), "party.leave.to-party")
            .build();

    // kick

    public Message playerKickedToKicked = Message.fromConfigurationSection(getLangConfiguration().getSection("party.kick.to-kicked"), "party.kick.to-kicked")
            .build();

    public Message playerKickedToParty = Message.fromConfigurationSection(getLangConfiguration().getSection("party.kick.to-party"), "party.kick.to-party")
            .build();

    // status error

    public Message noParty = Message.fromConfigurationSection(getLangConfiguration().getSection("party.status-error.no-party"), "party.status-error.no-party")
            .build();

    public Message noPartyExist = Message.fromConfigurationSection(getLangConfiguration().getSection("main.party-no-exist"), "main.party-no-exist")
            .build();

    public Message notLeader = Message.fromConfigurationSection(getLangConfiguration().getSection("party.status-error.not-leader"), "party.status-error.not-leader")
            .build();

    public Message alreadyInParty = Message.fromConfigurationSection(getLangConfiguration().getSection("party.status-error.already-in-party"), "party.status-error.already-in-party")
            .build();

    public Message alreadyLeader = Message.fromConfigurationSection(getLangConfiguration().getSection("party.status-error.already-leader"), "party.status-error.already-leader")
            .build();

    // chat

    public Message partyChat = Message.fromConfigurationSection(getLangConfiguration().getSection("party.chat.toggle"), "party.chat.toggle")
            .build();

    public Message chatReset = Message.fromConfigurationSection(getLangConfiguration().getSection("party.chat.reset"), "party.chat.reset")
            .build();

    // misc

    public Message transferLeader = Message.fromConfigurationSection(getLangConfiguration().getSection("party.transfer"), "party.transfer")
            .build();

    public Message partyInfo = Message.fromConfigurationSection(getLangConfiguration().getSection("party.info"), "party.info")
            .build();

    public Message hijack = Message.fromConfigurationSection(getLangConfiguration().getSection("party.hijack"), "party.hijack")
            .build();

    public Message disband = Message.fromConfigurationSection(getLangConfiguration().getSection("party.disband"), "party.disband")
            .build();

    /* Admin */

    // main
    public Message adminMain = Message.fromConfigurationSection(getLangConfiguration().getSection("admin.main"), "admin.main")
            .build();

    // reload success
    public Message reloadSuccess = Message.fromConfigurationSection(getLangConfiguration().getSection("admin.reload.success"), "admin.reload.success")
            .build();

    // reload failure
    public Message reloadFailure = Message.fromConfigurationSection(getLangConfiguration().getSection("admin.reload.failure"), "admin.reload.failure")
            .build();

    /**
     * Gets the lang configuration from the main class
     *
     * @return The lang configuration
     */
    private Configuration getLangConfiguration() {
        return QualityParties.getInstance().getLangConfig().getConfig();
    }
}

