package com.qualityseries.qualityparites.internal;

import dev.spaceseries.spaceapi.abstraction.plugin.BukkitPlugin;
import dev.spaceseries.spaceapi.command.BukkitSpaceCommandSender;
import dev.spaceseries.spaceapi.command.SpaceCommandSender;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class SpacePlugin {

    /**
     * SpaceAPI plugin
     */
    private final BukkitPlugin plugin;

    /**
     * Construct space plugin
     *
     * @param plugin plugin
     */
    public SpacePlugin(JavaPlugin plugin) {
        this.plugin = new BukkitPlugin(plugin);
    }

    /**
     * Returns spaceAPI plugin
     *
     * @return plugin
     */
    public BukkitPlugin getPlugin() {
        return plugin;
    }

    /**
     * Gets a player from a SpaceCommandSender
     *
     * @param spaceCommandSender The command sender
     */
    public Player getPlayer(SpaceCommandSender spaceCommandSender) {
        if (!spaceCommandSender.isPlayer()) return null;
        return Bukkit.getPlayer(spaceCommandSender.getUuid());
    }


    public SpaceCommandSender getSender(CommandSender sender) {
        return new BukkitSpaceCommandSender(sender);
    }

}

