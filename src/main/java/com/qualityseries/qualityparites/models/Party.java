package com.qualityseries.qualityparites.models;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

public class Party {

    private final UUID uuid;
    private Player leader;
    private final ArrayList<Player> members;

    public Party(Player leader) {
        this.uuid = UUID.randomUUID();
        this.leader = leader;
        this.members = new ArrayList<>();
    }

    public Party(Player leader, Player... members) {
        this.uuid = UUID.randomUUID();
        this.leader = leader;
        this.members = new ArrayList<>(Arrays.asList(members));
    }

    public ArrayList<Player> getMembers() {
        return members;
    }

    public Player getLeader() {
        return leader;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setLeader(Player leader) {
        this.leader = leader;
    }

    public ArrayList<Player> getAll() {
        ArrayList<Player> players = new ArrayList<>(members);
        players.add(leader);
        return players;
    }

    public void addMembers(Player... players) {
        members.addAll(Arrays.asList(players));
    }
    public void removeMembers(Player... players) {
        members.removeAll(Arrays.asList(players));
    }
    public void transferLeader(Player player) {
        if (!members.contains(player)) return;
        removeMembers(player);
        setLeader(player);
        addMembers(leader);
    }


}
