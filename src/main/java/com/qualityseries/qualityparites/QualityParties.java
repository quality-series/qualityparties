package com.qualityseries.qualityparites;

import com.jonahseguin.drink.CommandService;
import com.jonahseguin.drink.Drink;
import com.qualityseries.qualityparites.cmds.PartyCMD;
import com.qualityseries.qualityparites.cmds.PartySubCMD;
import com.qualityseries.qualityparites.cmds.QualityPartiesCMD;
import com.qualityseries.qualityparites.cmds.QualityPartiesSubCMD;
import com.qualityseries.qualityparites.config.Config;
import com.qualityseries.qualityparites.config.LangConfig;
import com.qualityseries.qualityparites.internal.SpacePlugin;
import com.qualityseries.qualityparites.listeners.ChatHandler;
import com.qualityseries.qualityparites.models.Party;
import com.qualityseries.qualityparites.util.PartyManager;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public final class QualityParties extends JavaPlugin {

    // QualityParties instance
    private static QualityParties instance;

    // Map of all current active parties
    private HashMap<UUID, Party> parties;

    // Map of pending invites for each player
    private HashMap<Player, ArrayList<Party>> pendingInvites;

    // config.yml configuration class instance
    private Config partyConfig;

    // lang.yml configuration class instance
    private LangConfig langConfig;

    // All SpaceAPI method location
    private SpacePlugin spacePlugin;

    // List of players with party chat *toggled* on
    private ArrayList<Player> partyChatEnabled;

    // PartyManager instance
    private PartyManager partyManager;

    @Override
    public void onLoad() { instance = this; }

    @Override
    public void onEnable() {
        spacePlugin = new SpacePlugin(this);

        loadConfigurations();
        loadCommands();
        loadEvents();

        partyManager = new PartyManager();

        parties = new HashMap<>();
        pendingInvites = new HashMap<>();
        partyChatEnabled = new ArrayList<>();
    }

    @Override
    public void onDisable() {}

    public void loadConfigurations() {
        langConfig = new LangConfig();
        partyConfig = new Config();
    }

    public void loadMessages() {
        Messages.renew();
    }

    private void loadCommands() {
        CommandService drink = Drink.get(this);
        drink.register(new PartyCMD(), "party", "p").registerSub(new PartySubCMD());
        drink.register(new QualityPartiesCMD(), "qualityparties", "qp").registerSub(new QualityPartiesSubCMD());
        drink.registerCommands();
    }

    private void loadEvents() {
        getServer().getPluginManager().registerEvents(new ChatHandler(), this);
    }

    public static QualityParties getInstance() {
        return instance;
    }

    public HashMap<UUID, Party> getParties() {
        return parties;
    }

    public HashMap<Player, ArrayList<Party>> getPendingInvites() {
        return pendingInvites;
    }

    public Config getPartyConfig() {
        return partyConfig;
    }

    public LangConfig getLangConfig() {
        return langConfig;
    }

    public SpacePlugin getSpacePlugin() {
        return spacePlugin;
    }

    public ArrayList<Player> getPartyChatEnabled() {
        return partyChatEnabled;
    }

    public PartyManager getPartyManager() {
        return partyManager;
    }
}
