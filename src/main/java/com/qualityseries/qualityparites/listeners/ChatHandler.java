package com.qualityseries.qualityparites.listeners;

import com.qualityseries.qualityparites.Messages;
import com.qualityseries.qualityparites.QualityParties;
import com.qualityseries.qualityparites.config.Config;
import com.qualityseries.qualityparites.models.Party;
import com.qualityseries.qualityparites.util.PartyManager;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static com.qualityseries.qualityparites.config.Config.*;

public class ChatHandler implements Listener {

    @EventHandler
    public void playerChat(AsyncPlayerChatEvent event) {
        if (QualityParties.getInstance().getPartyChatEnabled().contains(event.getPlayer())) {
            PartyManager pm = QualityParties.getInstance().getPartyManager();
            // Not a real chat message, so cancel the event. Still cancel just in case the message was meant only for party to see
            event.setCancelled(true);
            if (pm.getPartyByPlayer(event.getPlayer()) != null) {
                Party party = pm.getPartyByPlayer(event.getPlayer());
                party.getAll().forEach(target -> target.sendMessage(PlaceholderAPI.setPlaceholders(event.getPlayer(),
                        CHAT_FORMAT_STRUCTURE.get(Config.get())).replace("{0}", event.getMessage())));
                return;
            }
            QualityParties.getInstance().getPartyChatEnabled().remove(event.getPlayer());
            Messages.getInstance().chatReset.msg(QualityParties.getInstance().getSpacePlugin().getSender(event.getPlayer()));
        }
    }

}

