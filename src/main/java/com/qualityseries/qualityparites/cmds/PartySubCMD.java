package com.qualityseries.qualityparites.cmds;

import com.jonahseguin.drink.annotation.*;
import com.qualityseries.qualityparites.Messages;
import com.qualityseries.qualityparites.QualityParties;
import com.qualityseries.qualityparites.models.Party;
import com.qualityseries.qualityparites.util.PartyManager;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PartySubCMD {

    @Command(name = "accept", aliases = "join", desc = "Accept a pending party invite", usage = "[player]")
    @Require("qualityparties.user")
    public void acceptSub(@Sender Player player, @OptArg Player leader) {
        if (leader == null) {
            QualityParties.getInstance().getPartyManager().acceptInvite(player);
        } else {
            QualityParties.getInstance().getPartyManager().acceptInvite(player, QualityParties.getInstance().getPartyManager().getPartyByLeader(leader));
        }
    }

    @Command(name = "decline", aliases = {"reject", "deny"}, desc = "Decline a pending party invite", usage = "[player]")
    @Require("qualityparties.user")
    public void declineSub(@Sender Player player, @OptArg Player leader) {
        if (leader == null) {
            QualityParties.getInstance().getPartyManager().declineInvite(player);
        } else {
            QualityParties.getInstance().getPartyManager().declineInvite(player, QualityParties.getInstance().getPartyManager().getPartyByLeader(leader));
        }
    }

    @Command(name = "chat", aliases = "c", desc = "Toggle/Send messages current party", usage = "[message]")
    @Require("qualityparties.user")
    public void chatSub(@Sender Player player, @OptArg @Text String message) {
        PartyManager pm = QualityParties.getInstance().getPartyManager();

        if (pm.getPartyByPlayer(player) != null) {
            Party party = pm.getPartyByPlayer(player);
            if (message != null) {
                String rawFormat = QualityParties.getInstance().getPartyConfig().getConfig().getString("chat.party-format");
                party.getAll().forEach(target -> target.sendMessage(PlaceholderAPI.setPlaceholders(player, rawFormat).replace("{0}", message)));
            } else {
                // toggle party chat on/off
                if (QualityParties.getInstance().getPartyChatEnabled().contains(player)) {
                    Messages.getInstance().partyChat.msg(QualityParties.getInstance().getSpacePlugin().getSender(player), "{0}", "disabled");
                    QualityParties.getInstance().getPartyChatEnabled().remove(player);
                } else {
                    Messages.getInstance().partyChat.msg(QualityParties.getInstance().getSpacePlugin().getSender(player), "{0}", "enabled");
                    QualityParties.getInstance().getPartyChatEnabled().add(player);
                }
            }
        } else {
            Messages.getInstance().noParty.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
        }
    }

    @Command(name = "disband", desc = "Disbands a current party, if the executor is the leader")
    @Require("qualityparties.user")
    public void disbandSub(@Sender Player player) {
        QualityParties.getInstance().getPartyManager().disband(player);
    }

    @Command(name = "forcejoin", aliases = "fj", desc = "Allows users with permission to forcibly join parties", usage = "<player>")
    @Require("qualityparties.admin.forcejoin")
    public void forceJoinSub(@Sender Player player, Player target) {
        QualityParties.getInstance().getPartyManager().forceJoin(player, target);
    }

    @Command(name = "forceleader", aliases = "hijack", desc = "Allows users with permission to forcefully take leadership of the party")
    @Require("qualityparties.admin.forceleader")
    public void forceLeaderSub(@Sender Player player) {
        QualityParties.getInstance().getPartyManager().forceLeader(player);
    }

    @Command(name = "help", aliases = "?", desc = "Displays the main party help message")
    @Require("qualityparties.user")
    public void helpSub(@Sender CommandSender sender) {
        Messages.getInstance().partyHelp.msg(QualityParties.getInstance().getSpacePlugin().getSender(sender));
    }

    @Command(name = "invite", aliases = {"inv", "add"}, desc = "", usage = "<player>")
    @Require("qualityparties.user")
    public void inviteSub(@Sender Player leader, Player target) {
        QualityParties.getInstance().getPartyManager().invite(leader, target);
    }

    @Command(name = "kick", aliases = "remove", desc = "Kick a player from your party", usage = "<player>")
    @Require("qualityparties.user")
    public void kickSub(@Sender Player player, Player target) {
        QualityParties.getInstance().getPartyManager().kick(player, target);
    }

    @Command(name = "leave", aliases = "quit", desc = "Leave your current party")
    @Require("qualityparties.user")
    public void leaveSub(@Sender Player player) {
        QualityParties.getInstance().getPartyManager().leave(player);
    }

    @Command(name = "list", aliases = "info", desc = "Displays information about your current party")
    @Require("qualityparties.user")
    public void listSub(@Sender Player player) {
        QualityParties.getInstance().getPartyManager().info(player);
    }

    @Command(name = "transfer", aliases = "promote", desc = "Transfer leadership of your party to someone else", usage = "<player>")
    @Require("qualityparties.user")
    public void transferSub(@Sender Player player, Player target) {
        QualityParties.getInstance().getPartyManager().transfer(player, target);
    }

}

