package com.qualityseries.qualityparites.cmds;

import com.jonahseguin.drink.annotation.Command;
import com.jonahseguin.drink.annotation.Require;
import com.jonahseguin.drink.annotation.Sender;
import com.qualityseries.qualityparites.Messages;
import com.qualityseries.qualityparites.QualityParties;
import org.bukkit.command.CommandSender;

import java.util.concurrent.CompletableFuture;

public class QualityPartiesSubCMD {

    @Command(name = "reload", desc = "Reload the plugin files")
    @Require("qualityparties.admin.reload")
    public void reloadSub(@Sender CommandSender sender) {
        CompletableFuture.runAsync(() -> {
            try {
                QualityParties.getInstance().loadConfigurations();
                QualityParties.getInstance().loadMessages();
            } catch (Exception e) {
                Messages.getInstance().reloadFailure.msg(QualityParties.getInstance().getSpacePlugin().getSender(sender));
                e.printStackTrace();
                return;
            }
            Messages.getInstance().reloadSuccess.msg(QualityParties.getInstance().getSpacePlugin().getSender(sender));
        });
    }

}
