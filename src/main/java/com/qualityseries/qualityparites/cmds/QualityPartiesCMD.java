package com.qualityseries.qualityparites.cmds;

import com.jonahseguin.drink.annotation.Command;
import com.jonahseguin.drink.annotation.Require;
import com.jonahseguin.drink.annotation.Sender;
import com.qualityseries.qualityparites.Messages;
import com.qualityseries.qualityparites.QualityParties;
import org.bukkit.command.CommandSender;

public class QualityPartiesCMD {

    @Command(name = "", desc = "Main admin command")
    @Require("qualityparties.admin")
    public void roninPartiesCMD(@Sender CommandSender sender) {
        Messages.getInstance().adminMain.msg(QualityParties.getInstance().getSpacePlugin().getSender(sender));
    }

}

