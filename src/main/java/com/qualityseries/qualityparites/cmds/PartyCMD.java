package com.qualityseries.qualityparites.cmds;

import com.jonahseguin.drink.annotation.Command;
import com.jonahseguin.drink.annotation.OptArg;
import com.jonahseguin.drink.annotation.Require;
import com.jonahseguin.drink.annotation.Sender;
import com.qualityseries.qualityparites.Messages;
import com.qualityseries.qualityparites.QualityParties;
import org.bukkit.entity.Player;

public class PartyCMD {

    @Command(name = "", desc = "Main party command")
    @Require("qualityparties.user")
    public void party(@Sender Player player, @OptArg Player target) {
        // if the user didn't provide a player to invite, send the help message
        if (target == null) {
            Messages.getInstance().partyHelp.msg(QualityParties.getInstance().getSpacePlugin().getSender(player));
            return;
        }

        QualityParties.getInstance().getPartyManager().invite(player, target);
    }

}
