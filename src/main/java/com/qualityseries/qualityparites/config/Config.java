package com.qualityseries.qualityparites.config;

import com.qualityseries.qualityparites.QualityParties;
import dev.spaceseries.spaceapi.config.impl.Configuration;
import dev.spaceseries.spaceapi.config.keys.ConfigKey;
import dev.spaceseries.spaceapi.config.keys.ConfigKeyTypes;

public class Config extends dev.spaceseries.spaceapi.config.obj.Config {

    public Config() {
        super(QualityParties.getInstance().getSpacePlugin().getPlugin(), "config.yml");
    }

    public static ConfigKey<String> PLAYER_CHAT_STRUCTURE = ConfigKeyTypes.stringKey("chat.player-structure", "%vault_prefix%%player_name%");
    public static ConfigKey<String> CHAT_FORMAT_STRUCTURE = ConfigKeyTypes.stringKey("chat.party-format", "&9Party > %vault_prefix%%player_name%&7: &f{0}");

    public static Configuration get() {
        return QualityParties.getInstance().getPartyConfig().getConfig();
    }


}
