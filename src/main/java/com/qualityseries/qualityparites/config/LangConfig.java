package com.qualityseries.qualityparites.config;

import com.qualityseries.qualityparites.QualityParties;
import dev.spaceseries.spaceapi.config.obj.Config;

public class LangConfig extends Config {

    public LangConfig() {
        super(QualityParties.getInstance().getSpacePlugin().getPlugin(), "lang.yml");
    }

}
